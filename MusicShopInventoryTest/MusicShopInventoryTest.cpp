#include "pch.h"
#include "CppUnitTest.h"
#include "../OOP_Project_Musical_Instruments/Instrument.h"
#include "../OOP_Project_Musical_Instruments/Instrument.cpp"

#include "../OOP_Project_Musical_Instruments/StringInstrument.h"
#include "../OOP_Project_Musical_Instruments/StringInstrument.cpp"

#include "../OOP_Project_Musical_Instruments/WoodwindInstrument.h"
#include "../OOP_Project_Musical_Instruments/WoodwindInstrument.cpp"

#include "../OOP_Project_Musical_Instruments/InstrumentRepository.h"
#include "../OOP_Project_Musical_Instruments/InstrumentRepository.cpp"

#include "../OOP_Project_Musical_Instruments/InstrumentController.h"
#include "../OOP_Project_Musical_Instruments/InstrumentController.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace MusicShopInventoryTest
{
	TEST_CLASS(MusicShopInventoryTest)
	{
	public:
		
		TEST_METHOD(TestClassInstrument)
		{
			Instrument generic{1234, 50.6f};

			Assert::AreEqual(generic.getID(), 1234);
			generic.setID(123);
			Assert::AreEqual(generic.getID(), 123);

			Assert::AreEqual(generic.getPrice(), 50.6f);
			generic.setPrice(120.0);
			Assert::AreEqual(generic.getPrice(), 120.0f);
		}

		TEST_METHOD(TestStringInstrument) {
			StringInstrument si1{ 5556, 70, 4, "Stradivarius", "violin" };
			
			Assert::AreEqual(si1.getBrand(), string("Stradivarius"));
			si1.setBrand("Amati");
			Assert::AreEqual(si1.getBrand(), string("Amati"));

			Assert::AreEqual(si1.getNrStrings(), 4);
			si1.setNrStrings(3);
			Assert::AreEqual(si1.getNrStrings(), 3);

			Assert::AreEqual(si1.getType(), string("violin"));
			si1.setType("lute");
			Assert::AreEqual(si1.getType(), string("lute"));
		};

		TEST_METHOD(TestWoodwindInstrument) {
			WoodwindInstrument wi1{ 3214, 60, REED, "silver" };

			Assert::AreEqual(wi1.getMaterial(), string("silver"));
			wi1.setMaterial("gold");
			Assert::AreEqual(wi1.getMaterial(), string("gold"));

			/*Assert::AreEqual(wi1.getMeansSound(), REED);
			wi1.setMeansSound(FLUTE);
			Assert::AreEqual(wi1.getMeansSound(), FLUTE);*/
		}

		TEST_METHOD(TestInstrumentRepository) {
			Instrument generic{1234, 50.6f};
			StringInstrument si1{ 5556, 70, 4, "Stradivarius", "violin" };
			WoodwindInstrument wi1{ 3214, 60, REED, "silver" };
			InstrumentRepository repo;

			repo.addInstrument(&generic);
			repo.addInstrument(&si1);
			repo.addInstrument(&wi1);

			Assert::AreEqual(repo.size(), 3);

			repo.removeInstrument(1234);
			Assert::AreEqual(repo.size(), 2);

			cout << endl;
		}

		TEST_METHOD(TestInstrumentController) {
			Instrument generic{1234, 50.6f};
			StringInstrument si1{ 5556, 70, 4, "Stradivarius", "violin" };
			WoodwindInstrument wi1{ 3214, 60, REED, "silver" };
			InstrumentRepository repo;

			repo.addInstrument(&generic);
			repo.addInstrument(&si1);
			repo.addInstrument(&wi1);

			InstrumentController controller(repo);
			
			controller.removeByID(1234);
			Assert::AreEqual(controller.size(), 2);

			controller.undo();
			Assert::AreEqual(controller.size(), 3);

			controller.redo();
			Assert::AreEqual(controller.size(), 2);

			controller.addInstrument(&generic);
			Assert::AreEqual(controller.size(), 3);
		}
	};
}
