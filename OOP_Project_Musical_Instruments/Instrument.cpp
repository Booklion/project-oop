#include "Instrument.h"

Instrument::Instrument() {
	this->m_id = 0;
	this->m_price = 0.0f;
}

Instrument::Instrument(int id, float price) {
	this->m_id = id;
	this->m_price = price;
}

int Instrument::getID() const {
	return this->m_id;
}

void Instrument::setID(int newID) {
	try {
		if (isalpha(newID)) {
			throw exception();
		}
		else {
			this->m_id = newID;
		}
		
	}
	catch (int) {
		cout << "The ID introduced is not valid! Please try again!";
	}
}

float Instrument::getPrice() const {
	return this->m_price;
}

void Instrument::setPrice(float newPrice) {
	this->m_price = newPrice;
}

void Instrument::display(ostream& os) const {
	os << setw(30);
	os << std::left << "ID: " << m_id << endl;
	os << setw(30);
	os << std::left << "Price: " << m_price << endl;
}


ostream& operator<<(ostream& os, const Instrument& instrument)
{
	instrument.display(os);
	return os;
}
