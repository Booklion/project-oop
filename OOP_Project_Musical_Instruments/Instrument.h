/*****************************************************************//**
 * \file   Instrument.h
 * \brief  A regular instrument in the music shop is characterised by its ID and price
 * 
 * \author adors
 * \date   April 2021
 *********************************************************************/
#pragma once
#include<string>
#include<ostream> 
#include<istream> 
#include<iomanip>
#include<iostream>
#include<cctype>
using namespace std;

class Instrument {
public:
	/**
	 * Default constructor.
	 * 
	 * \return nothing
	 */
	Instrument();

	/**
	 * Constructor with parameters.
	 * 
	 * \param id : the ID of the instrument
	 * \param price : the price of the instrument
	 * \return nothing
	 */
	Instrument(int id, float price);

	/**
	 * Overloading the << operator.
	 * 
	 * \param os : the ostream parameter we use to store the output stream
	 * \param instrument : the instrument 
	 * \return the output stream
	 */
	friend ostream& operator<<(ostream& os, const Instrument& instrument);

	/**
	 * Getter for ID.
	 * \return the ID of the instrument
	 */
	int getID() const;
	
	/**
	 * Setter for ID.
	 */
	void setID(int newID);

	/**
	 * Getter for price.
	 * \return the price of the instrument
	 */
	float getPrice() const;

	/**
	 * Setter for price.
	 */
	void setPrice(float newPrice);

	/**
	 * Display method.
	 * 
	 * \param os : the ostream operator
	 */
	virtual void display(ostream& os) const;

	/**
	 * Saves the data into a .csv file.
	 * 
	 * \param outputFile : the name/path of the .csv file
	 */
	virtual void saveToCSV(ostream &outputFile) {
		outputFile << m_id << "," << m_price;
	}

private:
	/**
	 * the ID of an instrument is unique.
	 */
	int m_id;  
	float m_price;
};

