#include "InstrumentController.h"

InstrumentController::InstrumentController(InstrumentRepository& repository) {
	this->m_repository = repository;
}

void InstrumentController::addInstrument(Instrument* instrument) {
	this->m_repository.addInstrument(instrument);
	m_undoStack.push(std::make_pair(ActionType::ADD, instrument));
	//this->m_repository.iterateAndSave("musical_instruments_inventory.csv");
}

void InstrumentController::displayAll() {
	m_repository.display(cout, [](Instrument* i)->bool {
		cout << endl;
		return true; });
}

void InstrumentController::removeByID(int id) {
	Instrument* i = m_repository.removeInstrument(id);
	if (i) {
		m_undoStack.push(std::make_pair(ActionType::REMOVE, i));
	}
	//this->m_repository.iterateAndSave("musical_instruments_inventory.csv");
}

int InstrumentController::size() {
	return this->m_repository.size();
}

void InstrumentController::displayByPriceLowerThan(float price) {
	this->m_repository.displayByPriceLowerThan(price);
}

void InstrumentController::displayByType(string type) {
	this->m_repository.displayByType(type);
}

void InstrumentController::undo() {
	/**
	 *  How this wonder works:
	 * Possible operations to undo:
	 *  1) ADD :
	 * (i) move the pair (ADD, instrumentName) to the REDO STACK 
	 * (ii) delete it from the UNDO STACK
	 * (iii) remove instrumentName
	 * 
	 * 2) REMOVE :
	 * (i) move the pair (REMOVE, instrumentName) to the REDO STACK
	 * (ii) delete it from the UNDO STACK
	 * (iii) add instrumentName.
	 * 
	 */
	/// ADD:
	if (this->m_undoStack.top().first == ActionType::ADD) {
		/// (i)
		this->m_redoStack.push(std::make_pair(ActionType::ADD, this->m_undoStack.top().second));
		/// (ii)
		this->m_undoStack.pop();
		/// (iii)
		this->m_repository.removeInstrument(this->m_redoStack.top().second->getID());
	}
	/// REMOVE:
	if (this->m_undoStack.top().first == ActionType::REMOVE) {
		/// (i)
		this->m_redoStack.push(std::make_pair(ActionType::REMOVE, this->m_undoStack.top().second));
		/// (ii)
		this->m_undoStack.pop();
		/// (iii)
		this->m_repository.addInstrument(this->m_redoStack.top().second);
	}
	//this->m_repository.iterateAndSave("musical_instruments_inventory.csv");
}

void InstrumentController::redo() {
	/**
	 * How this other wonder works:
	 * Possible operations to redo:
	 * 1) ADD:
	 * (i) move the pair (ADD, instrumentName) to the UNDO STACK
	 * (ii) delete it from the REDO STACK
	 * (iii) add instrumentName
	 * 
	 * 2) REMOVE :
	 * (i) move the pair (REMOVE, instrumentName) to the UNDO STACK
	 * (ii) delete it from the REDO STACK
	 * (iii) remove instrumentName.
	 * 
	 */
	/// ADD:
	if (this->m_redoStack.top().first == ActionType::ADD) {
		/// (i)
		this->m_undoStack.push(std::make_pair(ActionType::ADD, this->m_redoStack.top().second));
		/// (ii)
		this->m_redoStack.pop();
		/// (iii)
		this->m_repository.addInstrument(this->m_undoStack.top().second);
	}
	if (this->m_redoStack.top().first == ActionType::REMOVE) {
		/// (i)
		this->m_undoStack.push(std::make_pair(ActionType::REMOVE, this->m_redoStack.top().second));
		/// (ii)
		this->m_redoStack.pop();
		/// (iii)
		this->m_repository.removeInstrument(this->m_undoStack.top().second->getID());
	}
	//this->m_repository.iterateAndSave("musical_instruments_inventory.csv");
}

void InstrumentController::read() {
	this->m_repository.readData("musical_instruments_inventory.csv");
}

void InstrumentController::save() {
	this->m_repository.iterateAndSave("musical_instruments_inventory.csv");
}
