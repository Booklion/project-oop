/*****************************************************************//**
 * \file   InstrumentController.h
 * \brief  This is the controller
 * 
 * \author adors
 * \date   April 2021
 *********************************************************************/
#pragma once
#include "InstrumentRepository.h"
#include<iostream>
#include<fstream>
#include<stack>
using namespace std;

enum class ActionType {
	ADD, 
	REMOVE
};

class InstrumentController {
public: 
	/**
	 * Constructor.
	 * 
	 * \param repository : the repository we store our objects in
	 * \return nothing
	 */
	InstrumentController(InstrumentRepository& repository);

	/**
	 * Add an instrument to the repository.
	 * 
	 * \param instrument : the instrument we want to add to the repository
	 */
	void addInstrument(Instrument* instrument);

	/**
	 * Display the content of the repository.
	 * 
	 */
	void displayAll();

	/**
	 * Remove an instrument by ID.
	 * 
	 * \param id : the ID of the instrument we want to remove
	 */
	void removeByID(int id);

	/**
	 * Displays the instruments having the price lower than a given value
	 * 
	 * \param price : the upper bound of the price
	 */

	/**
	 * Computes the number of elements in the shop.
	 * 
	 * \return the number of instruments
	 */
	int size();

	/**
	 * Display the instruments which have the price lower than a specified value.
	 * 
	 * \param price : the upper bound of the value of the price
	 */
	void displayByPriceLowerThan(float price);

	/**
	 * Display only objects of a certain type.
	 * 
	 * \param type : the type of the instrument
	 * s - StringInstrument
	 * w - WoodwindInstrument
	 * throws an exception if the type is neither "s" nor "w"
	 */
	void displayByType(string type);

	/**
	 * Undo the last action performed.
	 * if the last action was ADD, the instrument added will be removed
	 * if the last action was REMOVE, the instrument removed will be added back into the repository
	 *
	 */
	void undo();

	/**
	 * Redo the last action that was previously undone.
	 * if the action was REMOVE, the instrument which was added after the undo() will be removed once again into the repository
	 * if the last action was ADD, the instrument which was removed by the undo() will be added once again into the repository
	 *
	 */
	void redo();

	/**
	 * Read the data from the .csv file where the instruments are stored.
	 * 
	 */
	void read();

	/**
	 * Save the changes in the .csv file where the instruments are stored.
	 * 
	 */
	void save();

private: 
	InstrumentRepository m_repository;
	std::stack<pair<ActionType, Instrument*>> m_undoStack;
	std::stack<pair<ActionType, Instrument*>> m_redoStack;
};

