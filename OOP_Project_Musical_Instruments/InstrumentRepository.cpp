#include "InstrumentRepository.h"

InstrumentRepository::InstrumentRepository() {
	this->readData("musical_instruments_inventory.csv");
}

Instrument* InstrumentRepository::operator[](int i) {
	return this->m_repo[i];
}

void InstrumentRepository::addInstrument(Instrument* i) {
	m_repo.push_back(i);
}

Instrument* InstrumentRepository::removeInstrument(int id) {
	Instrument* instrument = nullptr;
	// we'll be using some interesting stuff to look for the element with the mentioned ID in the repository
	// we will return the index of the element with the given ID, if it exists
	auto index = find_if(m_repo.begin(), m_repo.end(), [id](Instrument* instrument)->bool {
		return instrument->getID() == id;
		});
	if (index != m_repo.end()) {
		// the index exists, so the instrument exists, and we delete it
		instrument = *index;
		m_repo.erase(index);
	}
	return instrument;
}

int InstrumentRepository::size() {
	return this->m_repo.size();
}

ostream& InstrumentRepository::display(ostream& os, bool(*function)(Instrument* i)) {
	for (auto i = 0; i < m_repo.size(); i++) {
		if (function(m_repo[i])) {
			os << *m_repo[i];
		}
	}
	return os;
}

void InstrumentRepository::displayByPriceLowerThan(float price) {
	int k = 0;
	for (size_t i = 0; i < m_repo.size(); i++) {
		if (m_repo[i]->getPrice() < price) {
			m_repo[i]->display(cout);
			k++;
			cout << endl;
		}
	}
	if (k == 0) {
		cout << "There were no instruments with the price under " << price << "!";
	}
}

void InstrumentRepository::displayByType(string type) {
	vector<Instrument*> validString;
	vector<Instrument*> validWoodwind;

	for (size_t i = 0; i < this->m_repo.size(); i++) {
		if (dynamic_cast<StringInstrument*>(this->m_repo[i])) {
			validString.push_back(this->m_repo[i]);
		}
		if (dynamic_cast<WoodwindInstrument*>(this->m_repo[i])) {
			validWoodwind.push_back(this->m_repo[i]);
		}
	}

	if (type == "s") {
		for (auto i = 0; i < validString.size(); i++) {
			validString[i]->display(cout);
			cout << endl;
		}
	}
	else {
		if (type == "w") {
			for (auto i = 0; i < validWoodwind.size(); i++) {
				validWoodwind[i]->display(cout);
				cout << endl;
			}
		}
		else {
			cerr << "Invalid command!";
		}
	}
}

bool InstrumentRepository::saveData(const char* csv_path, Instrument* i) {
	ofstream outputFile;

	outputFile.open(csv_path, fstream::out);

	outputFile << i->getID() << "," << i->getPrice() << ",";
	if (dynamic_cast<StringInstrument*>(i)) {
		outputFile << dynamic_cast<StringInstrument*>(i)->getBrand() << "," << dynamic_cast<StringInstrument*>(i)->getNrStrings() << "," << dynamic_cast<StringInstrument*>(i)->getType() << endl;
	}

	else {
		if (dynamic_cast<WoodwindInstrument*>(i)) {
			outputFile << dynamic_cast<WoodwindInstrument*>(i)->getMaterial() << ",";
			if (dynamic_cast<WoodwindInstrument*>(i)->getMeansSound() == REED) {
				outputFile << "REED" << endl;
			}

			if (dynamic_cast<WoodwindInstrument*>(i)->getMeansSound() == FLUTE) {
				outputFile << "FLUTE" << endl;
			}
		}
		else {
			outputFile << endl;
		}
	}
	outputFile.close();
	return true;
}

void InstrumentRepository::iterateAndSave(const char* csv_path) {
	ifstream oldFile(csv_path);
	ofstream outputFile("new_musical_instruments_inventory.csv");
	// todo checks

	for (auto i = 0; i < this->m_repo.size(); i++) {
		this->m_repo[i]->saveToCSV(outputFile);
		outputFile << endl;
	}
	if (!remove(csv_path) != 0) {
		perror("Failed to delete file! :c");
	}

	else {
		rename("new_musical_instruments_inventory.csv", csv_path);
	}
}

bool InstrumentRepository::readData(const char* csv_path) {
	ifstream inputFile;
	string data;

	inputFile.open(csv_path, fstream::in);

	do {
		// reads data from the file until we reach the end of the file (EOF == true)
		inputFile >> data;

	} while (!EOF); 

	inputFile.close();
	return true;
}
