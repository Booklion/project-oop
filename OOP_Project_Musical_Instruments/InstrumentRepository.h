/*****************************************************************//**
 * \file   InstrumentRepository.h
 * \brief  This is the instrument repository
 * 
 * \author adors
 * \date   April 2021
 *********************************************************************/
#pragma once
#include "Instrument.h"
#include "StringInstrument.h"
#include "WoodwindInstrument.h"
#include<ostream>
#include<vector>
#include<iostream>
#include<fstream>
#include<cstdio>
using namespace std;

class InstrumentRepository {
public:
	/**
	 * Default constructor.
	 * Reads data from the .csv file
	 * 
	 */
	InstrumentRepository();

	/**
	 * Overload for the indexing/subscript operator.
	 * 
	 * \param i : the index of the element we want to get
	 * \return a reference to the i-th elementin the array
	 */
	Instrument* operator[](int i);

	/**
	 * Add an instrument to the repository.
	 * 
	 * \param i : the instrument we want to add to the repository
	 */
	void addInstrument(Instrument* i);

	/**
	 * Remove instrument by ID.
	 * 
	 * \param id : the ID of the element we want to delete
	 * \return the instrument we deleted
	 */
	Instrument* removeInstrument(int id);

	/**
	 * Computes the size of the repository.
	 * 
	 * \return the number of intruments in the repository
	 */
	int size();

	/**
	 * Display method - displays an instrument based on a property.
	 * 
	 * \param os : the ostream operator we will use to display the instrument oject
	 * \param function : the property the instrument shall fulfill in order to be eligible for display
	 * \return the instrument in the way described in the function
	 */
	ostream& display(ostream& os, bool (*function)(Instrument* i));

	/**
	 * Display all instruments which have the price lower than a given value.
	 * 
	 * \param price : the value we compare the prices to
	 */
	void displayByPriceLowerThan(float price);

	/**
	 * Displays all the instruments of a certain type.
	 * 
	 * \param type : the type of the instruments
	 * s - string
	 * w - woodwind
	 * throws an exception if type is neither "s" nor "w"
	 */
	void displayByType(string type);

	/**
	 * Read data from a .csv file.
	 *
	 * \param csv_path : the path of the .csv file
	 * \return true if the data has been successfully read from the file
	 */
	static bool readData(const char* csv_path);

	/**
	 * Save the data in a .csv file.
	 *
	 * \param csv_path : the path of the .csv file
	 * \param i : the data itself
	 * \return true if the data has been successfully saved
	 */
	bool saveData(const char* csv_path, Instrument* i);

	/**
	 * Save all data from the repository in a .csv file.
	 * 
	 */
	void iterateAndSave(const char* csv_path);

private:
	vector<Instrument*> m_repo;
};

