#include "StringInstrument.h"

StringInstrument::StringInstrument() {
	this->m_nrStrings = 0;
	this->m_type = ' ';
	this->m_brand = ' ';
}

StringInstrument::StringInstrument(int id, float price, int nrStrings, string brand, string type) : Instrument(id, price) {
	this->m_nrStrings = nrStrings;
	this->m_brand = brand;
	this->m_type = type;
}

int StringInstrument::getNrStrings() const {
	return this->m_nrStrings;
}

void StringInstrument::setNrStrings(int newNrStrings) {
	try {
		if (isalpha(newNrStrings)) {
			throw exception();
		}
		else {
			this->m_nrStrings = newNrStrings;
		}
	}
	catch (int) {
		cout << "The number of strings is invalid! Please try again!";
	}
}

string StringInstrument::getBrand() const {
	return this->m_brand;
}

void StringInstrument::setBrand(string newBrand) {
	this->m_brand = newBrand;
}

string StringInstrument::getType() const {
	return this->m_type;
}

void StringInstrument::setType(string newType) {
	this->m_type = newType;
}

void StringInstrument::display(ostream& os) const {
	Instrument::display(os);
	os << setw(30);
	os << "Nr. strings: " << m_nrStrings << endl;
	os << setw(30);
	os << "Type: " << m_type  << endl;
	os << setw(30);
	os << "Brand: " << m_brand << endl;
}

ostream& operator<<(ostream& os, const StringInstrument& sinstrument) {
	sinstrument.display(os);
	return os;
}
