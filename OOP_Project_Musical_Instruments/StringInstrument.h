/*****************************************************************//**
 * \file   StringInstrument.h
 * \brief  A string instrument has three extra attributes, aside from the ones of a regular instrument: the number of chords/strings, the brand and thy type of the instrument (violin, contrabass etc)
 * 
 * \author adors
 * \date   April 2021
 *********************************************************************/
#pragma once
#include "Instrument.h"
#include<cctype>
#include<iostream>
using namespace std;

class StringInstrument : public Instrument {
public:
	/**
	 * Default constructor.
	 * 
	 * \return nothing
	 */
	StringInstrument();

	/**
	 * Constructor with parameters.
	 * 
	 * \param id : the ID of the instrument (inherited from the Instrument class)
	 * \param price : the price of the instrument (inherited from the Instrument class)
	 * \param nrStrings : the number of chords/strings of the instrument
	 * \param brand : the brand of the instrument
	 * \param type : the type of the instrument (violin, lute, contrabass etc)
	 * \return nothing
	 */
	StringInstrument(int id, float price, int nrStrings, string brand, string type);

	/**
	 * Overloading the  << operator.
	 * \param os : the ostream parameter we use to store the output stream
	 * \param sinstrument : the instrument 
	 * \return the output stream
	 */
	friend ostream& operator<<(ostream& os, const StringInstrument& sinstrument);

	/**
	 * Getter for the number of strings.
	 * \return the number of strings of the instrument
	 */
	int getNrStrings() const;

	/**
	 * Setter for the number of strings.
	 */
	void setNrStrings(int newNrStrings);

	/**
	 * Getter for the brand.
	 * 
	 * \return the brand of the instrument
	 */
	string getBrand() const;

	/**
	 * Setter for the brand. 
	 */
	void setBrand(string newBrand);

	/**
	 * Getter for type.
	 * 
	 * \return the type of the instrument
	 */
	string getType() const;

	/**
	 * Setter for type.
	 * 
	 * \return the type of the instrument
	 */
	void setType(string newType);

	/**
	 * Display method - override for the one in the Instrument class.
	 * \param os : the ostream operator
	 */
	void display(ostream& os) const override;

	/**
	 * Override for the saveToCSV function from the base class Instrument.
	 * 
	 * \param outputFile : the name/path of the .csv file
	 */
	void saveToCSV(ostream& outputFile) override {
		Instrument::saveToCSV(outputFile);
		outputFile << "," << m_nrStrings << "," << m_brand << "," << m_type;
	}

private: 
	int m_nrStrings;
	string m_brand;
	string m_type;
};

