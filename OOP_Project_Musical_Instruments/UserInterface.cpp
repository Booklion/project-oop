#include "UserInterface.h"

UserInterface::UserInterface(InstrumentController& controller) : m_controller{ controller }
{
}

void UserInterface::add() {
	int id;
	float price;
	cout << "Enter the ID: "; cin >> id;
	cout << endl;
	cout << "Enter the price: "; cin >> price;
	cout << endl;
	
	cout << "Do you want to add a string instrument or a woodwind instrument?" << endl;
	cout << "\t y - Yes";
	cout << endl;
	cout << "\t n - No";
	cout << endl;

	char option1;
	char option2;

	cout << "Option: "; cin >> option1;
	cout << endl;

	if (option1 == 'y') {
		cout << "String instrument or woodwind instrument?" << endl;
		cout << "\t s - String insturment" << endl;
		cout << "\t w - Woodwind instrument" << endl;

		cout << "Your option: "; cin >> option2;
		cout << endl;
		if (option2 == 's') {
			string brand;
			int nrStrings;
			string type;
			cout << "Enter the brand: "; cin >> brand;
			cout << endl;
			cout << "Enter the number of strings: "; cin >> nrStrings;
			cout << endl;
			cout << "Enter the type: "; cin >> type;
			cout << endl;

			StringInstrument* sinstrument = new StringInstrument{ id, price, nrStrings, brand, type };
			m_controller.addInstrument(sinstrument);
		}
		if (option2 == 'w') {
			string material;
			string smeansSound;

			cout << "Enter the material: "; cin >> material;
			cout << endl;
			cout << "Enter the means of producing sound category: "; cin >> smeansSound;
			cout << endl;
			 
			TypeSound meansSound;
			if (smeansSound == "reed") {
				meansSound = REED;
			}
			else {
				if (smeansSound == "flute") {
					meansSound = FLUTE;
				}
				else {
					cerr << "Invalid type!";
				}
			}

			WoodwindInstrument* winstrument = new WoodwindInstrument{ id, price, meansSound, material };
			m_controller.addInstrument(winstrument);
		}

	}
	else if (option1 == 'n') {
		Instrument* instrument = new Instrument{ id, price };
		m_controller.addInstrument(instrument);
	}
	else {
		cout << "Invalid option! Please try again!";
	}
}

void UserInterface::remove() {
	int id;
	cout << "Enter the ID of the instrument you want to remove: "; cin >> id;
	m_controller.removeByID(id);
}

void UserInterface::showMenu() {
	char option;
	do {
		this->m_controller.read();
		cout << endl;
		cout << "Hello, welcome to our shop! Please enter your option from the list below: " << endl;
		cout << "\t a - Add instrument" << endl;
		cout << "\t r - Remove an instrument by ID" << endl;
		cout << "\t d - Display all instruments" << endl;
		cout << "\t f - Filter instruments" << endl;
		cout << "\t u - Undo" << endl;
		cout << "\t z - Redo" << endl;
		cout << "\t e - Exit" << endl;
		cin >> option;
		switch (option) {
		default:
			break;

		case'a':
			add();
			break;

		case'r':
			remove();
			break;

		case'd':
			m_controller.displayAll();
			break;

		case'f':
			int optionFilter;
			cout << "What particular instruments do you want to see?" << endl;
			cout << "Here are your options: " << endl;
			cout << "\t 1 - Instruments having the price lower than a given price" << endl;
			cout << "\t 2 - Instruments of a given type" << endl;
			cout << "Your option: "; cin >> optionFilter;
			cout << endl;
			try {
				if (optionFilter == 1) {
					float price;
					cout << "Enter the maximum price: "; cin >> price;
					cout << endl;
					this->m_controller.displayByPriceLowerThan(price);
					break;
				}
				if (optionFilter == 2) {
					string type;
					cout << "Here are the types of instruments we currently have in our shop: " << endl;
					cout << "\t s - String Instruments" << endl;
					cout << "\t w - Woodwind Instruments" << endl;
					cout << "Enter an option (s for string instruments and w for woodwing instruments): "; cin >> type;
					this->m_controller.displayByType(type);
					break;
				}
			}
			catch (...) {
				throw exception("Invalid command!");
			}

		case'u':
			m_controller.undo();

		case'z':
			m_controller.redo();

		case'e':
			this->m_controller.save();
			cout << "Thank you for coming! See you again soon!" << endl;
			exit(0);
			break;
		}

	} while (true);
	cout << endl;
}

void UserInterface::displayByLowerPrice(float price) {
	this->m_controller.displayByPriceLowerThan(price);
}

void UserInterface::displayByType(string type) {
	this->m_controller.displayByType(type);
}
