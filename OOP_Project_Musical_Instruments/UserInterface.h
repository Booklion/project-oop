/*****************************************************************//**
 * \file   UserInterface.h
 * \brief  This is the user interface. Here we display the menu and we perform certain operations on the objects
 * 
 * \author adors
 * \date   April 2021
 *********************************************************************/
#pragma once
#include "InstrumentController.h"

class UserInterface {
public: 
	/**
	 * Constructor.
	 * 
	 * \param controller : the controller used to perform operations on the repository
	 * \return nothing
	 */
	UserInterface(InstrumentController& controller);

	/**
	 * Add an instrument.
	 *
	 */
	void add();

	/**
	 * Remove an instrument.
	 * 
	 */
	void remove();

	/**
	 * Display the menu.
	 * 
	 */
	void showMenu();

	/**
	 * Displays the instrument from the repo which has the price lower than a given value.
	 * 
	 * \param price : the upper bound of the price
	 */
	void displayByLowerPrice(float price);

	/**
	 * Displays the instruments from the repo which are of a given type.
	 * 
	 * \param type : the type of the instruments
	 * s - StringInstrument
	 * w - WoodwindInstrument
	 * throws an exception if type is neither "s" nor "w"
	 */
	void displayByType(string type);

private:
	InstrumentController m_controller;
};

