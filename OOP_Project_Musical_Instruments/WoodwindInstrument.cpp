#include "WoodwindInstrument.h"

std::map<TypeSound, string> WoodwindInstrument::m_typeConvertor = {
	{REED, "reed"},
	{FLUTE, "flute"}
}
;
WoodwindInstrument::WoodwindInstrument() {
	this->m_meansSound = REED;
	this->m_material = ' ';
}

WoodwindInstrument::WoodwindInstrument(int id, float price, TypeSound meansSound, string material) : Instrument(id, price) {
	this->m_meansSound = meansSound;
	this->m_material = material;
}

TypeSound WoodwindInstrument::getMeansSound() const {
	return this->m_meansSound;
}

void WoodwindInstrument::setMeansSound(TypeSound newMeansSound) {
	this->m_meansSound = newMeansSound;
}

string WoodwindInstrument::getMaterial() const {
	return this->m_material;
}

void WoodwindInstrument::setMaterial(string newMaterial) {
	this->m_material = newMaterial;
}

void WoodwindInstrument::display(ostream& os) const {
	Instrument::display(os);
	os << setw(30);
	os << "Category: " << this->m_typeConvertor[m_meansSound] << endl;
	os << setw(30);
	os << "Material: " << m_material << endl;
}

ostream& operator<<(ostream& os, const WoodwindInstrument& winstrument) {
	winstrument.display(os);
	return os;
}
