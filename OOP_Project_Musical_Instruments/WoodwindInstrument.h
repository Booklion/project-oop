/*****************************************************************//**
 * \file   WoodwindInstrument.h
 * \brief  A woodwind instrument has two extra attributes, aside from the ID and price of a normal instrument: the material it is made of and the means of producing sound (type enum)
 * 
 * \author adors
 * \date   April 2021
 *********************************************************************/
#pragma once
#include "Instrument.h"
#include<array>
#include<cctype>
#include<iostream>
#include<map>
using namespace std;

enum TypeSound {
	REED,
	FLUTE
};

class WoodwindInstrument : public Instrument {
public:
	/**
	 * Default constructor.
	 * 
	 * \return nothing
	 */
	WoodwindInstrument();

	/**
	 * Constructor with parameters.
	 * 
	 * \param id : the ID of the instrumetn (inherited from the Instrument class)
	 * \param price : the price of the instrument (inherited from the Instrument class)
	 * \param meansSound : the means of producing sound
	 * \param material : the material the instrument is made out of
	 * \return nothing 
	 */
	WoodwindInstrument(int id, float price, TypeSound meansSound, string material);

	/**
	 * Overloading the  << operator.
	 * \param os : the ostream parameter we use to store the output stream
	 * \param sinstrument : the instrument
	 * \return the output stream
	 */
	friend ostream& operator<<(ostream& os, const WoodwindInstrument& winstrument);

	/**
	 * Getter for the means of producing sound.
	 * 
	 * \return the means of producing sound
	 */
	TypeSound getMeansSound() const;

	/**
	 * Setter for means of producing sound.
	 * 
	 */
	void setMeansSound(TypeSound newMeansSound);

	/**
	 * Getter for material.
	 * 
	 * \return the material the instrument is made out of
	 */
	string getMaterial() const;

	/**
	 * Setter for material.
	 * 
	 */
	void setMaterial(string newMaterial);

	/**
	 * Display method - override for the function in the Instrument class.
	 * \param os : the ostream operator 
	 */
	void display(ostream& os) const override;

	/**
	 * Override for the saveToCSV function in the base class Instrument.
	 * 
	 * \param outputFile : the name/path of the .csv file
	 */
	void saveToCSV(ostream& outputFile) override {
		Instrument::saveToCSV(outputFile);
		outputFile << "," << m_material << "," << m_typeConvertor[m_meansSound];
	}
private: 
	static std::map<TypeSound, string> m_typeConvertor;
	TypeSound m_meansSound;
	string m_material;
};

