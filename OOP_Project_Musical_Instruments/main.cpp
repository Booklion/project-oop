#include "Instrument.h"
#include "StringInstrument.h"
#include "WoodwindInstrument.h"
#include "InstrumentRepository.h"
#include "InstrumentController.h"
#include "UserInterface.h"
#include<iostream>
using namespace std;


int main() {
	cout << "Testing Instrument:" << endl;
	cout << endl;
	Instrument generic{4269, 30.6f};
	
	cout << "Generic: " << endl;
	generic.display(cout);
	cout << endl;
	cout << "The old ID is: " << generic.getID();
	generic.setID(123);
	cout << endl;
	cout << "The new ID is:" << generic.getID() << endl;
	cout << "The old price is: " << generic.getPrice() << endl;
	generic.setPrice(120); 
	cout << "The new price is: " << generic.getPrice() << endl;

	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	cout << endl;


	cout << "Testing StringInstrument:" << endl;
	cout << endl;
	StringInstrument si1{ 5556, 70, 4, "Stradivarius", "violin" };

	si1.display(cout);
	cout << endl;
	cout << "The old brand is: " << si1.getBrand() << endl;
	si1.setBrand("Amati");
	cout << "The new brand is: " << si1.getBrand() << endl;
	cout << "The old number of strings is: " << si1.getNrStrings() << endl;
	si1.setNrStrings(3);
	cout << "The new number of strings is: " << si1.getNrStrings() << endl;
	cout << "The old type is: " << si1.getType() << endl;
	si1.setType("lute");
	cout << "The new type is: " << si1.getType() << endl;

	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	cout << endl;

	cout << "Testing WoodwindInstrument:" << endl;
	cout << endl;
	WoodwindInstrument wi1{ 1234, 50, FLUTE, "wood" };

	wi1.display(cout);
	cout << endl;
	cout << "The old material is: " << wi1.getMaterial() << endl;
	wi1.setMaterial("gold");
	cout << "The new material is: " << wi1.getMaterial() << endl;
	cout << "The old category of means of producing sound is: " << wi1.getMeansSound() << endl;
	wi1.setMeansSound(FLUTE);
	cout << "The new category of means of producing sound is: " << wi1.getMeansSound() << endl;
	cout << endl;

	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	cout << endl;

	cout << "Testing InstrumentRepository:" << endl;
	cout << endl;
	InstrumentRepository repo;
	repo.addInstrument(&generic);
	repo.addInstrument(&si1);
	repo.addInstrument(&wi1);

	repo.iterateAndSave("instruments.csv");

	repo.display(cout, [](Instrument* i)->bool {
		if (dynamic_cast<StringInstrument*>(i)) {
			return true;
		}
		return false;
		});
	cout << endl;

	repo.removeInstrument(1234);

	repo.displayByPriceLowerThan(70);
	cout << endl;
	
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	cout << endl;


	cout << "Testing InstrumentController:" << endl;
	InstrumentController controller(repo);
	controller.displayAll();
	cout << endl;

	controller.removeByID(1234);
	controller.displayAll();
	cout << endl;

	controller.addInstrument(&generic);
	controller.displayAll();
	cout << endl;

	UserInterface ui{ controller };

	ui.showMenu();

	return 0;
}