## Musical instruments

Implemented functions:

- [x] Getters & setters for all attributes of every class
- [x] Add
- [x] Remove by ID
- [x] Display all instruments
- [x] Filter instruments by their type (display only string or woodwind instrument)
- [x] Display all instruments which have the price lower than a given value
- [x] Persistence
- [x] Undo
- [x] Redo
 

## _Classes:_
- **Instrument** (_base class_): 
    - _ID_ (m_id): the ID of the instrument (unique for each instrument)
    - _price_ (m_price): the price of an instrument


- **StringInstrument** (_derived from Instrument_): Aside from the ID and price inherited from the class Instrument, a StringInstrument object has as attributes: 
    - *number of strings* (m_nrStrings): how many strings such an instrument has
    - _brand_ (m_brand): the manufacturer of the instrument
    - _type_ (m_type): what instrument this really is (ex: a violin, a lute, a contrabass etc)


- **WoodwindInstrument** (_derived from Instrument_): Aside from the ID and price inherited from the class Instrument, a WoodwindInstrument object has as attributes: 
    - _means of producing sound_ (m_meansSound): the category in which the woodwind instrument falls into based on its acoustic properties (REED or FLUTE)
    - _material_ (m_material): the material it is made out of (ex: gold, silver etc.)


- **InstrumentRepository**: The collection of instruments of all types


- **InstrumentController**


- **UserInterface**
    
